import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
import time


from rubish_automated_utils import StepCount
from rubish_automated_utils import Seq


class Ultrasonic_sensor(object):
    """This class helps us to measure the distance using ultrasonic sensor HC-SRO4"""
    def __init__(self, trigger_pin_number: InterruptedError, echo_pinout:int) -> None:
        # Take the pin numbers
        self.trigger_pin_number = trigger_pin_number
        self.echo_pinout = echo_pinout

        GPIO.setup(self.trigger_pin_number, GPIO.OUT) # Trgier as OUTPUT
        GPIO.setup(self.echo_pinout, GPIO.IN) # Echo pin as input 


    # Ultrasonic sensor methodes
    def measure_distance(self):
        # set Trigger to HIGH
        GPIO.output(self.trigger_pin_number, True)
    
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(self.trigger_pin_number, False)
    
        StartTime = time.time()
        StopTime = time.time()
    
        # save StartTime
        while GPIO.input(self.echo_pinout) == 0:
            StartTime = time.time()
    
        # save time of arrival
        while GPIO.input(self.echo_pinout) == 1:
            StopTime = time.time()
    
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
    
        return distance

    # end ---------------------------------------------


class Stepper_motor(object):
    def __init__(self, 
                 coil_A_1_pin,
                 coil_A_2_pin,
                 coil_B_1_pin,
                 coil_B_2_pin,
                 coil2_A_1_pin,
                 coil2_A_2_pin,
                 coil2_B_1_pin,
                 coil2_B_2_pin
                 ) -> None:
    # Take the pin number         
        self.coil_A_1_pin = coil_A_1_pin
        self.coil_A_2_pin = coil_A_2_pin
        self.coil_B_1_pin = coil_B_1_pin
        self.coil_B_2_pin = coil_B_2_pin
        self.coil2_A_1_pin = coil2_A_1_pin
        self.coil2_A_2_pin = coil2_A_2_pin
        self.coil2_B_1_pin = coil2_B_1_pin         
        self.coil2_B_2_pin = coil2_B_2_pin
        self.coil_A_1_pin = coil_A_1_pin

        # Set all pins as output
        GPIO.setup(self.coil_A_1_pin, GPIO.OUT)
        GPIO.setup(self.coil_A_2_pin, GPIO.OUT)
        GPIO.setup(self.coil_B_1_pin, GPIO.OUT)
        GPIO.setup(self.coil_B_2_pin, GPIO.OUT)
        GPIO.setup(self.coil2_A_1_pin, GPIO.OUT)
        GPIO.setup(self.coil2_A_2_pin, GPIO.OUT)
        GPIO.setup(self.coil2_B_1_pin, GPIO.OUT)
        GPIO.setup(self.coil2_B_2_pin, GPIO.OUT)


        
        # Stepper motor functions 
    def stepper_motor_setStep(self,w1, w2, w3, w4):

        # Set pins as output
        GPIO.output(self.coil_A_1_pin, w1)
        GPIO.output(self.coil2_A_1_pin, w1)
        GPIO.output(self.coil_A_2_pin, w2)
        GPIO.output(self.coil2_A_2_pin, w2)
        GPIO.output(self.coil_B_1_pin, w3)
        GPIO.output(self.coil2_B_1_pin, w3)
        GPIO.output(self.coil_B_2_pin, w4)
        GPIO.output(self.coil2_B_2_pin, w4)

    def stepper_motor_forward(self, delay, steps):
        for i in range(steps):
            for j in range(StepCount):
                self.stepper_motor_setStep(Seq[j][0], Seq[j][1], Seq[j][2], Seq[j][3])
                time.sleep(delay)

    def stepper_motor_backwards(self, delay, steps):
        for i in range(steps):
            for j in reversed(range(StepCount)):
                self.stepper_motor_setStep(Seq[j][0], Seq[j][1], Seq[j][2], Seq[j][3])
                time.sleep(delay)
        # end -------------------------------------------------------
class State:
    ON = "HIGH"
    OFF = "LOW"

class LED(object):
    def __init__(self, red_led_pin_number: str , green_led_pin_number: str):
        self.red_led_pin_number_class = red_led_pin_number
        self.green_led_pin_number_class = green_led_pin_number
        
        GPIO.setup(self.red_led_pin_number_class, GPIO.OUT, initial= GPIO.LOW) # Set gpio as output
        GPIO.setup(self.green_led_pin_number_class, GPIO.OUT, initial= GPIO.LOW) # Set gpio as output
        
        self.red = LEDRGB(color="red", red_led_pin_number=red_led_pin_number, green_led_pin_number=green_led_pin_number)
        self.green = LEDRGB(color="green", red_led_pin_number=red_led_pin_number, green_led_pin_number=green_led_pin_number)
        

class LEDRGB(object):
    def __init__(self, color:str, red_led_pin_number:int, green_led_pin_number:int ) -> None:
        self.color = color
        self.red_led_pin_number = red_led_pin_number
        self.green_led_pin_number = green_led_pin_number

    
    def output_state(self, state: State):
        if self.color == "red":
            if state == State.OFF:
                GPIO.output(self.green_led_pin_number,GPIO.LOW) 
                GPIO.output(self.red_led_pin_number,GPIO.LOW) 

            if state == State.ON:
                GPIO.output(self.green_led_pin_number,GPIO.LOW) 
                GPIO.output(self.red_led_pin_number,GPIO.HIGH) 
        
        if self.color == "green":
            if state == State.OFF:
                GPIO.output(self.red_led_pin_number,GPIO.LOW) 
                GPIO.output(self.green_led_pin_number,GPIO.LOW)  
            if state == State.ON:
                GPIO.output(self.red_led_pin_number,GPIO.LOW) 
                GPIO.output(self.green_led_pin_number,GPIO.HIGH)  


