import RPi.GPIO as GPIO
import time
 

from rubish_automated import LED, State
from rubish_automated import Ultrasonic_sensor
from rubish_automated import Stepper_motor

from rubish_automated_utils import PINOUT_LED_RED
from rubish_automated_utils import PINOUT_LED_GREEN

from rubish_automated_utils import PINOUT_ULTRASONIC_ECHO
from rubish_automated_utils import PINOUT_ULTRASONIC_SENSOR_TRIGGER

from rubish_automated_utils import PINOUT_coil2_A_1_pin
from rubish_automated_utils import PINOUT_coil2_A_2_pin
from rubish_automated_utils import PINOUT_coil2_B_1_pin
from rubish_automated_utils import PINOUT_coil2_B_2_pin
from rubish_automated_utils import PINOUT_coil_A_1_pin
from rubish_automated_utils import PINOUT_coil_A_2_pin
from rubish_automated_utils import PINOUT_coil_B_1_pin
from rubish_automated_utils import PINOUT_coil_B_2_pin

# Constantes
delay_forward = 1
delay_backward = 10
steps = 120

GPIO.setwarnings(False)

if __name__ == '__main__':
    led = LED(red_led_pin_number=PINOUT_LED_RED, 
              green_led_pin_number=PINOUT_LED_GREEN) # Instantiate the class 
    
    ultrasonic_sensor = Ultrasonic_sensor(echo_pinout=PINOUT_ULTRASONIC_ECHO, 
                                          trigger_pin_number=PINOUT_ULTRASONIC_SENSOR_TRIGGER)
    
    stepper_motor = Stepper_motor(coil_A_1_pin = PINOUT_coil_A_1_pin,
                                  coil_A_2_pin = PINOUT_coil_A_2_pin,
                                  coil_B_1_pin = PINOUT_coil_B_1_pin,
                                  coil_B_2_pin = PINOUT_coil_B_2_pin,
                                  coil2_A_1_pin = PINOUT_coil2_A_1_pin,
                                  coil2_A_2_pin = PINOUT_coil2_A_2_pin,
                                  coil2_B_1_pin = PINOUT_coil2_B_1_pin,
                                  coil2_B_2_pin = PINOUT_coil2_B_2_pin
                                 )

    
    try:
        while True:
            led.green.output_state(state=State.ON)
            dist = ultrasonic_sensor.measure_distance()
            print ("Measured Distance = %.1f cm" % dist)

            if dist <20:
                led.red.output_state(state=State.ON)
                stepper_motor.stepper_motor_forward(delay = int(delay_forward) / 1000.0, steps=int(steps))
                time.sleep(5)
                stepper_motor.stepper_motor_backwards(int(delay_backward) / 1000.0, int(steps))
            time.sleep(1)
 
    # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")