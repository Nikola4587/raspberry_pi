
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

# LED pinout
PINOUT_LED_RED = 6
PINOUT_LED_GREEN = 5


# Ultrasonic sensor pinout
PINOUT_ULTRASONIC_SENSOR_TRIGGER = 16
PINOUT_ULTRASONIC_ECHO = 20


#Stepper motor 
PINOUT_coil_A_1_pin = 24 # pink
PINOUT_coil_A_2_pin = 4 # orange
PINOUT_coil_B_1_pin = 23 # blau
PINOUT_coil_B_2_pin = 25 # gelb
PINOUT_coil2_A_1_pin = 18 # pink
PINOUT_coil2_A_2_pin = 22 # orange
PINOUT_coil2_B_1_pin = 17 # blau
PINOUT_coil2_B_2_pin = 27 # gelb
StepCount = 8
Seq = list(range(0, StepCount))
Seq[0] = [0,1,0,0]
Seq[1] = [0,1,0,1]
Seq[2] = [0,0,0,1]
Seq[3] = [1,0,0,1]
Seq[4] = [1,0,0,0]
Seq[5] = [1,0,1,0]
Seq[6] = [0,0,1,0]
Seq[7] = [0,1,1,0]
